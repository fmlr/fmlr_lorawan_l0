/**
  ******************************************************************************
  * @file    hw_eeprom.h
  * @author  Alex Raimmondi <raimondi@miromico.ch>
  * @brief   This file provides eeprom memory related functions.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _HW_EEP_H
#define _HW_EEP_H

#include <stdbool.h>

/* Exported functions ------------------------------------------------------- */
/*!
 * \brief Writes byte to dst in eeprom
 *
 * \param [OUT] dst  Destination in eeprom
 * \param [IN]  data Data to write
 */
bool HW_EEP_write_byte(const uint8_t* dst, const uint8_t data);

/*!
 * \brief Writes size elements of src array to dst array in eeprom
 *
 * \param [OUT] dst  Destination array in eeprom
 * \param [IN]  src  Source array
 * \param [IN]  size Number of bytes to be copied
 */
bool HW_EEP_write_buffer(const uint8_t* dst, const uint8_t* src, uint16_t size);

#endif
