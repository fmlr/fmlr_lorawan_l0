/******************************************************************************
  * @file    bsp_version.c
  * @author  Alex Raimondi
  * @version V1.0.2
  * @date    15-November-2016
  * @brief   BSP version handling
  ******************************************************************************
  */

#include "bsp_version.h"
#include "hw.h"

/** Get region name */
static const char* GetRegionName(void);

__weak void printApplicationInformation() {}

void printSystemInformation(const char* name, uint32_t stack, const char* app)
{
  PRINTF("*** %s ***" NL, name);

#if defined(FMLR72_L0)
  PRINTF("Module:        FMLR72_L0" NL);
#elif defined(CMWX1ZZABZ)
  PRINTF("Module:        CMWX1ZZABZ" NL);
#else
#error "Define module"
#endif
  PRINTF("Stack version: %X" NL, stack);
  PRINTF("BSP version:   %s" NL, BSP_VERSION_STRING);
  PRINTF("APP version:   %s" NL, app);
  PRINTF("Freq region:   %s" NL, GetRegionName());
  printApplicationInformation();
  PRINTF(NL);
  PRINTD(NL "DEBUG IS ENABLED" NL);
}

static const char* GetRegionName()
{
#if defined(REGION_AS923)
  return "AS923";
#elif defined(REGION_AU915)
  return "AU915";
#elif defined(REGION_IN865)
  return "IN865";
#elif defined(REGION_EU868)
  return "EU868";
#elif defined(REGION_KR920)
  return "KR920";
#elif defined(REGION_US915)
  return "US915";
#elif defined(REGION_US915_HYBRID)
  return "US915_HYBRID";
#endif
}
