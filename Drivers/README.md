# Drivers Library

This is the Miromico LoRa module BSP and driver package. It includes all low level hardware drivers for different modules and MCU

## Version history

V1.3.0

Supports: Murata, FMLR-STL1

- All changes from stack update to latest LoRaWAN stack 1.1.4 from ST
- Introduce new "end of line" macro NL.
- Added SHT31 implementation
- Added SIC4311 NFC chip
- Added mcp23017 I2C GPIO expander
- Added Post B2B button lcd
- Moved some interrupt vectors into common code

V1.2.0

**Only works with Murata module (no support for Miromico FMLR yet)**

- Moved common includes into project
- Fixed missing clock initialization in UART init
- linker script changes
- Flash
- IAP
- Vector table relocation
- I2c: disable during sleep
- fixed OID initialization

V1.1.2

- Moved BSP/Common includes into project. Each project needs to include the common modules in hw_conf.
- Use define to disable/enable USART and SPI busses
- Add preprocessor symbol for appication that need to relocate vector table
- Add common module for CRC
- Add common module for MCU flash access
- Add RamFunc section to linker files
- Add common module for eeprom
- Rework in i2C

V1.1.1

- Support for Murata module and Miromico FMLR
- Support for various sensensors and components

Dependency
- Middelwares V1.1.2.4
