/******************************************************************************
  * @file    lora_test.h
  * @author  MCD Application Team
  * @version V1.1.4
  * @date    08-January-2018
  * @brief   lora API to drive the lora state Machine
  ******************************************************************************
  */

#ifndef __LORA_TEST_H__
#define __LORA_TEST_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

#define CERTIF_PORT 224
/* Exported types ------------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
bool certif_running(void);

void certif_DownLinkIncrement(void);

void certif_linkCheck(MlmeConfirm_t* mlmeConfirm);

void certif_rx(McpsIndication_t* mcpsIndication, MlmeReqJoin_t* JoinParameters);

#ifdef __cplusplus
}
#endif

#endif /*__LORA_TEST_H__*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
